#include "pgm4.h"
#include "alt_types.h"
#include "sys/alt_timestamp.h"

int main()
{
	int h,w;
	int i,j,k,temp;
    double divi;
    alt_u32 time1,time2;
	unsigned char** data;
    unsigned char** data2;
    int filter[3][3];
	printf("Hello from Nios II!\n");


	data = pgmread("/mnt/host/inputImage.pgm",&h,&w);

    //creamos la matriz que guardará la imagen modificada
    data2 = (unsigned char**)calloc((w-2), sizeof(unsigned char*));
    for (j=0 ; j < (w-2); j++){
        data2[j] = (unsigned char*)calloc((h-2), sizeof(unsigned char));
    }
    
    
    //inicializamos valor del filtro enfoque
    filter[0][0] = 0;
    filter[0][1] = -1;
    filter[0][2] = 0;
    filter[1][0] = -1;
    filter[1][1] = 5;
    filter[1][2] = -1;
    filter[2][0] = 0;
    filter[2][1] = -1;
    filter[2][2] = 0;
    /*
    //inicializamos valor del filtro desenfoque 
    filter[0][0] = 1;
    filter[0][1] = 1;
    filter[0][2] = 1;
    filter[1][0] = 1;
    filter[1][1] = 1;
    filter[1][2] = 1;
    filter[2][0] = 1;
    filter[2][1] = 1;
    filter[2][2] = 1;
    
    //inicializamos valor del filtro repujado 
    filter[0][0] = -2;
    filter[0][1] = -1;
    filter[0][2] = 0;
    filter[1][0] = -1;
    filter[1][1] = 1;
    filter[1][2] = 1;
    filter[2][0] = 0;
    filter[2][1] = 1;
    filter[2][2] = 2;
    */
    //calculamos divisor
    temp = 0;
    for(i=0;i<3;i++){
        for(j=0;j<3;j++){
        temp += filter[i][j];
        }
    }
    divi = (1/(double)temp);

    //realizamos el mismo filtro varias veces, para obtener un valor medio
    for(k=0;k<4;k++){
		//iniciamos el contador
    	alt_timestamp_start();
		time1 = alt_timestamp();

		//aplicamos el filtro
		for(j=1;j<h-1;j++){//filas
			for(i=1;i<w-1;i++){//columnas
				temp =0;
				temp += filter[0][0]*((int)data[i-1][j-1]);
				temp += filter[0][1]*((int)data[i][j-1]);
				temp += filter[0][2]*((int)data[i+1][j-1]);
				temp += filter[1][0]*((int)data[i-1][j]);
				temp += filter[1][1]*((int)data[i][j]);
				temp += filter[1][2]*((int)data[i+1][j]);
				temp += filter[2][0]*((int)data[i-1][j+1]);
				temp += filter[2][1]*((int)data[i][j+1]);
				temp += filter[2][2]*((int)data[i+1][j+1]);
				temp = (int)(((double)temp)/divi);
				data2[i-1][j-1]= temp;
			}
		}

		//detenemos el timer y imprimimos el resultado
		time2 = alt_timestamp();
		printf("time: %u\n",(unsigned int) (time2-time1));
    }
    //guardamos el resultado
	if(pgmwrite("/mnt/host/inputImageOUTPUT.pgm",h-2,w-2,data2,NULL,1) == 0){
		printf("File succesfully saved");
	}else{
		printf("Error saving file");
	}

	return 0;
}
